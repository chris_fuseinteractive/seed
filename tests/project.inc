<?php

class SeedProjectTest {
  /**
   * Tests the creation and deletion of projects.
   */
  public function testProjectCreateAndDelete() {
    $project = new SeedProject('a_test_project', $_SERVER['USER']);
    $project->create_dir();

    SeedTest::assert($project->dir_exists(), TRUE);

    $project->delete();

    SeedTest::assert($project->dir_exists(), FALSE);
  }

  /**
   * Tests the creation a bare Git repository in a project.
   */
  public function testProjectBare() {
    $project = new SeedProject('a_test_project', $_SERVER['USER']);
    $project->create_dir();
    $project->bare_repository(FALSE, FALSE);

    SeedTest::assertFileExists($project->dir . '/.git/config');

    $project->delete();
  }

  /**
   * Tests the cloning of a Git repository in to a project.
   */
  public function testProjectClone() {
    $project = new SeedProject('a_test_project', $_SERVER['USER']);
    $project->create_dir();
    $project->clone_repository('https://bitbucket.org/evan_fuseinteractive/seed_test.git');

    SeedTest::assertFileExists($project->dir . '/README');

    $project->delete();
  }

  /**
   * Tests the copying of projects between users.
   */
  public function testProjectCopy() {
    $from_project = new SeedProject('a_test_project', $_SERVER['USER']);
    $from_project->create_dir();
    file_put_contents($from_project->dir . '/TEST', 'testProjectCopy');

    $to_project = new SeedProject('another_test_project', $_SERVER['USER']);
    $to_project->copy($from_project);

    SeedTest::assert(file_get_contents($to_project->dir . '/TEST'), 'testProjectCopy');

    $from_project->delete();
    $to_project->delete();
  }

  /**
   * Tests branching of a Git repository in a project.
   */
  public function testProjectBranches() {
    $project = new SeedProject('a_test_project', $_SERVER['USER']);
    $project->create_dir();
    $project->clone_repository('https://bitbucket.org/evan_fuseinteractive/seed_test.git');

    SeedTest::assert($project->get_branch(), 'master');
    SeedTest::assert($project->check_status(), TRUE);

    $project->create_feature_branch('test');
    $project->switch_branch('test');
    SeedTest::assert($project->get_branch(), 'test');

    $project->switch_branch('master');
    SeedTest::assert($project->delete_feature_branch('test'), TRUE);

    $project->delete();
  }
}
