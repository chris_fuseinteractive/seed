<?php

/**
 * Implementation of hook_seed_pre_sync().
 */
function checklist_seed_pre_sync($from_alias, $to_alias, $selection) {

  if(stripos($to_alias, 'live') === FALSE || !drush_confirm(dt('Run launch checklist?'))) {
    return;
  }

  $funcs = get_defined_functions();

  drush_log(dt('[Checklist] Running checklist tests ...'), 'ok');

  foreach ($funcs['user'] as $func) {
    if (substr($func, 0, 14) === 'checklist_test') {
      $func($from_alias, $to_alias, $selection);
    }
  }

  $results = checklist_result();

  // Print results.
  $error_count = 0;
  foreach ($results as $result) {
    $error_count += (int) ($result['status'] == 'error');
    drush_log('[Checklist] ' . $result['message'], $result['status']);
  }

  if ($error_count && drush_confirm(dt('Detected !count checklist failures. Abort sync?', array('!count' => $error_count)))) {
    return drush_set_error('SEED_SYNC_CHECKLIST_ABORT', dt('[Checklist] Sync aborted.'));
  }
}

/**
 * Results cache.
 */
function checklist_result($entry = NULL) {
  static $result = NULL;

  if ($result === NULL) {
    $result = array();
  }

  if (!$entry) {
    return $result;
  }

  $result[] = $entry;
}

/**
 * Look for a Google Analytics code.
 */
function checklist_test_google_anayltics($from_alias, $to_alias, $selection) {
  drush_shell_exec("drush $from_alias vget googleanalytics_account");
  $ga = NULL;
  foreach (drush_shell_exec_output() as $line) {
    if (stripos($line, 'googleanalytics_account') !== FALSE) {
      $ga = $line;
      break;
    }
  }

  $result = (strlen($ga))
    ? array('status' => 'ok', 'message' => dt('!var', array('!var' => $ga)))
    : array('status' => 'error', 'message' => dt('Could not find a Google Anayltics code'));

  checklist_result($result);
}

/**
 * Check to see if CSS compression is on.
 */
function checklist_test_preprocess_css($from_alias, $to_alias, $selection) {
  drush_shell_exec("drush $from_alias vget preprocess_css");
  $preprocess_css = FALSE;
  foreach (drush_shell_exec_output() as $line) {
    if (stripos($line, 'preprocess_css: 1') !== FALSE) {
      $preprocess_css = TRUE;
      break;
    }
  }

  $result = ($preprocess_css)
    ? array('status' => 'ok', 'message' => dt('CSS compression is on'))
    : array('status' => 'error', 'message' => dt('CSS compression is off'));

  checklist_result($result);
}

/**
 * Check to see if JS compression is on.
 */
function checklist_test_preprocess_js($from_alias, $to_alias, $selection) {
  drush_shell_exec("drush $from_alias vget preprocess_js");
  $preprocess_js = FALSE;
  foreach (drush_shell_exec_output() as $line) {
    if (stripos($line, 'preprocess_js: 1') !== FALSE) {
      $preprocess_js = TRUE;
      break;
    }
  }

  $result = ($preprocess_js)
    ? array('status' => 'ok', 'message' => dt('JS compression is on'))
    : array('status' => 'error', 'message' => dt('JS compression is off'));

  checklist_result($result);
}

/**
 * Check to see if UID 1's password is their username.
 */
function checklist_test_superuser_password($from_alias, $to_alias, $selection) {
  // Get UID 1's username.
  drush_shell_exec("drush $from_alias uinf 1");
  $username = '';
  foreach (drush_shell_exec_output() as $line) {
    if (stripos($line, 'User name') !== FALSE) {
      $username = trim(array_pop(explode(':', $line)));
    }
  }

  // Get the hashed password from the DB.
  drush_shell_exec("drush $from_alias sqlq \"SELECT pass FROM users WHERE name = '$username'\"");
  $password = array_pop(drush_shell_exec_output());

  //
  $empty = array();
  $from = drush_sitealias_evaluate_path($from_alias, $empty);
  include_once $from['path'] . '/includes/password.inc';

  $account = new stdClass;
  $account->pass = $password;
  $result = (!user_check_password($username, $account))
    ? array('status' => 'ok', 'message' => dt('UID 1\'s password has been changed'))
    : array('status' => 'error', 'message' => dt('UID 1\'s password matches the username'));

  checklist_result($result);
}

/**
 * Check to see if there's more than one user.
 */
function checklist_test_user_count($from_alias, $to_alias, $selection) {
  drush_shell_exec("drush $from_alias sqlq \"SELECT COUNT(*) FROM users\"");
  $count = array_pop(drush_shell_exec_output());

  // We check for > 2 as there is UID 0 for anon. users.
  $result = (intval($count) > 2)
    ? array('status' => 'ok', 'message' => dt('More than one user'))
    : array('status' => 'error', 'message' => dt('Only one user account created'));

  checklist_result($result);
}

/**
 * Check to see if the site_mail variable has been changed.
 */
function checklist_test_site_mail($from_alias, $to_alias, $selection) {
  drush_shell_exec("drush $from_alias vget site_mail");

  $result = (stripos(array_pop(drush_shell_exec_output()), 'fuse') === FALSE)
    ? array('status' => 'ok', 'message' => dt('Site email does not appear to be a Fuse address'))
    : array('status' => 'error', 'message' => dt('Site email appears to be a Fuse address'));

  checklist_result($result);
}

/**
 * Check the status of the "from" alias' Features.
 */
function checklist_test_from_features($from_alias, $to_alias, $selection) {
  _checklist_check_feature_status($from_alias);
}

/**
 * Check the status of the "to" alias' Features.
 */
function checklist_test_to_features($from_alias, $to_alias, $selection) {
  _checklist_check_feature_status($to_alias);
}

/**
 * Checks the Features status for an alias.
 */
function _checklist_check_feature_status($alias) {
  drush_shell_exec("drush $alias features-list");
  $output = drush_shell_exec_output();
  $output = array_slice($output, 1, -1);

  $overridden = array();

  foreach ($output as $feature) {
    // Remove multiple spaces.
    $feature = preg_replace('!\s+!', ' ', $feature);
    // Can now split by a single space.
    $parts = explode(" ", $feature);

    $state_or_status = array_pop($parts);
    if ($state_or_status === 'Disabled') {
      continue;
    }
    else if ($state_or_status === 'Overridden') {
      // This will always be "Enabled".
      array_pop($parts);
      $overridden[] = array_pop($parts);
    }
  }

  $result = (!count($overridden))
    ? array('status' => 'ok', 'message' => dt('No overridden Features for !alias', array('!alias' => $alias)))
    : array('status' => 'error', 'message' => dt('The following features are overridden for !alias: !features', array('!alias' => $alias, '!features' => implode(", ", $overridden))));

  checklist_result($result);
}