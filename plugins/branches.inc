<?php

/**
 * Implements hook_seed_clone_complete().
 */
function branches_seed_clone_complete($dir) {
  drush_shell_cd_and_exec($dir, 'git branch -a');
  $output = drush_shell_exec_output();
  $branches = array();
  foreach ($output as $line) {
    if (stripos($line, 'master') === FALSE) {
      $branches[] = array_pop(explode('/', $line));
    }
  }

  if (!count($branches)) {
    return;
  }

  $choice = drush_choice($branches, dt('Switch to alternative branch?'));
  if ($choice !== FALSE) {
    drush_shell_cd_and_exec($dir, 'git checkout ' . $branches[$choice]);
  }
}