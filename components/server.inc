<?php

/**
 * Command callback for `drush seed-start`.
 */
function drush_seed_start() {
  $server = seed_get_server();

  $vhosts = $server->get_vhosts();
  if (empty($vhosts)) {
    return drush_set_error('DRUSH_SERVER_NO_VHOSTS', dt('Create at least one virtual host using drush vhost-add before you can start the server.'));
  }

  // Check server config, hopefully anything that prevent the server from
  // starting will be resolved here.
  drush_seed_check_config();

  if ($server->start()) {
    drush_log(dt('Server started with the following virtual hosts:'), 'ok');
    drush_seed_list();
  }
  else {
    return drush_set_error('SEED_SERVER_START_ERROR', dt('Unable to start the server.'));
  }
}

/**
 * Command callback for `drush seed-stop`.
 */
function drush_seed_stop() {
  $server = seed_get_server();

  if ($server->stop()) {
    drush_log(dt('Server stopped.'), 'ok');
  }
  else {
    return drush_set_error('SEED_SERVER_STOP_ERROR', dt('Unable to stop the server.'));
  }
}

/**
 * Command callback for `drush seed-restart`.
 */
function drush_seed_restart() {
  $server = seed_get_server();

  exec('curl --data "$USER ran seed restart" https://fuse.slack.com/services/hooks/slackbot?token=JqGLRQByVL3TuBMcJ0DpqDYp\&channel=%23general');

  // $server->stop();
  drush_seed_stop();
  while ($server->is_running()) {
    sleep(1);
  }
  drush_seed_start();
}

/**
 * Command callback for `drush seed-status`.
 */
function drush_seed_status() {
  $server = seed_get_server();

  $vhosts = $server->get_vhosts();
  if (empty($vhosts)) {
    return drush_set_error('DRUSH_SERVER_NO_VHOSTS', dt('Create at least one virtual host using drush vhost-add before you can start the server.'));
  }

  if ($server->is_running()) {
    drush_log(dt('The server is running, with these virtual hosts:'), 'ok');
    drush_seed_list();
  }
  else {
    drush_log(dt('The server is stopped.'), 'ok');
  }
}

/**
 * Command callback for `drush seed-check-config`.
 */
function drush_seed_check_config() {
  $server = seed_get_server();

  $status = drush_shell_exec($server->apachectl("-t"));
  if (!$status) {
    drush_log(dt('The following errors were found in the server configuration:'), 'error');

    $output = drush_shell_exec_output();
    seed_util_error_print($output);
    return;
  }

  $vhosts = $server->get_vhosts();
  $rows = array();
  foreach ($vhosts as $vhost) {
    $disable = FALSE;
    if (!is_dir($vhost->doc_root)) {
      $disable = TRUE;
      $rows[] = array($vhost->host, 'DocumentRoot missing', $vhost->doc_root);
    }

    if (!file_exists($vhost->error_log)) {
      $disable = TRUE;
      $rows[] = array($vhost->host, 'Error log missing', $vhost->error_log);
    }

    if (!file_exists($vhost->access_log)) {
      $disable = TRUE;
      $rows[] = array($vhost->host, 'Access log missing', $vhost->access_log);
    }

    if ($disable) {
      $vhost->disable();
    }
  }

  if (count($rows)) {
    array_unshift($rows, array('Virtual host', 'Error', 'Current value'));
    drush_print_table($rows);
    return;
  }

  drush_log(dt('No errors found in the server configuration.'), 'ok');
}

/**
 * Command callback for `drush seed-error-log`.
 */
function drush_seed_error_log() {
  $server = seed_get_server();
  $log_file = $server->log_path .'/error_log';
  drush_shell_exec_interactive("tail -f {$log_file}");
}

/**
 * Command callback for `drush seed-list`.
 */
function drush_seed_list() {
  $server = seed_get_server();

  $vhosts = $server->get_vhosts();
  if (empty($vhosts)) {
    drush_log(dt('No virtual host configurations found.'), 'warning');
    return;
  }

  ksort($vhosts);

  foreach ($vhosts as $name => $vhost) {
    $rows[] = array(
      $vhost->host,
      '->',
      $vhost->doc_root,
    );
  }

  drush_print_table($rows);
}

/**
 * Command callback for `drush seed-add`.
 */
function drush_seed_add($project = NULL) {
  $server = seed_get_server();
  $user = $_SERVER['USER'];

  if (empty($project)) {
    $project = drush_prompt('Project Name');
  }

  $config = seed_get_config(array('/\[PROJECT\]/' => $project, '/\[USER\]/' => $user));

  $host = drush_prompt('Host name', $config['server_vhost_template']);
  $uri = 'http://'. $host;

  $vhost = $server->vhost_get($uri);
  if ($vhost->exists()) {
    return drush_set_error('DRUSH_TOOLBOX_VHOST_EXISTS', dt('The specified virtual host already exists.'));
  }

  $doc_root = $config['project_directory'];

  $vhost->doc_root = drush_prompt('Document root', $doc_root);

  $vhost->save();
  drush_log("[Added] {$vhost->host} -> {$vhost->doc_root}", 'ok');

  // Now that the file has been written out to disk, we need to load() it again
  // and do some extra processing work on it.
  $vhost->load();
  $logs = array($vhost->error_log, $vhost->access_log);
  foreach ($logs as $log_file) {
    // If the file doesn't exist, write it so apache won't quit.
    if (!file_exists($log_file)) {
      $parts = explode('/', $log_file);
      $file = array_pop($parts);
      $dir = '';
      foreach($parts as $part) {
        if(!is_dir($dir .= "/$part")) {
          drush_mkdir($dir);
        }
      }
      file_put_contents("$dir/$file", '');
    }
  }

  if ($server->is_running()) {
    drush_prompt('The server will be restarted.', 'Hit <enter> to continue.');
    drush_seed_restart();
    drush_log('Server restarted.', 'ok');
  }
}

/**
 * Command callback for `drush seed-remove`.
 */
function drush_seed_remove($project = NULL) {
  $server = seed_get_server();
  $user = $_SERVER['USER'];

  if (empty($project)) {
    $project = drush_prompt('Project Name');
  }

  $config = seed_get_config(array('/\[PROJECT\]/' => $project, '/\[USER\]/' => $user));

  $host = drush_prompt('Host name', $config['server_vhost_template']);
  $uri = 'http://'. $host;

  $vhost = $server->vhost_get($uri);
  if (!$vhost->exists()) {
    return drush_set_error('DRUSH_TOOLBOX_VHOST_DOES_NOT_EXIST', dt('The specified virtual host configuration does not exist.'));
  }

  if (!drush_confirm(dt('Delete configuration for !uri?', array('!uri' => $vhost->uri)))) {
    // Cancelled.
    return;
  }

  $vhost->delete();
  drush_log(dt('!host removed.', array('!host' => $vhost->host)), 'ok');

  if ($server->is_running()) {
    drush_seed_restart();
    drush_log('Server restarted.', 'ok');
  }
}
