<?php

seed_include('user.inc');

/**
 * Command callback for `drush seed-user-add`.
 */
function drush_seed_user_add($username = NULL) {
  if (empty($username)) {
    $username = drush_prompt(dt('User name'));
  }

  $sudo = drush_confirm(dt('Grant sudo rights?'));

  $user = new SeedUser($username);
  $user->create($sudo);

  if (drush_confirm(dt('Add a MySQL user?'))) {
    drush_seed_db_user_add($username);
  }

  if (drush_confirm(dt('Add a SSH key?'))) {
    drush_seed_user_add_key($user);
  }
}

/**
 * Command callback for `drush seed-user-delete`.
 */
function drush_seed_user_delete($username = NULL) {
  if (empty($username)) {
    $username = drush_prompt(dt('User name'));
  }

  $delete_home = drush_confirm(dt("Delete user's home directory?"));
  if ($delete_home) {
    $backup_home = drush_confirm(dt('Backup home directory before deletion?'));
  }

  if (!drush_confirm(dt('Final confirmation: Are you sure you wish to delete the user !user?', array('!user' => $username)))) {
    return drush_set_error('SEED_USER_DELETE_BAILED', dt('Deletion of user !user was cancelled.', array('!user' => $username)));
  }

  $user = new SeedUser($username);

  seed_invoke('seed_user_pre_delete', $user->name);

  $user->delete($delete_home, $backup_home);

  if (drush_confirm(dt('Delete MySQL user?'))) {
    drush_seed_db_user_delete($username);
  }

  seed_invoke('seed_user_deleted', $user->name);
}

/**
 * Command callback for `drush seed-user-add-key`.
 */
function drush_seed_user_add_key($user = NULL) {
  if (is_string($user)) {
    $user = new SeedUser($user);
  }
  else if (!is_object($user) || (is_object($user) && get_class($user) !== 'SeedUser')) {
    $user = drush_prompt(dt('User name'));
    $user = new SeedUser($user);
  }

  $alias = drush_prompt(dt('Alias name'));
  $host = drush_prompt(dt('Hostname for this alias'));

  $user->add_key($alias, $host);

  seed_invoke('seed_user_key_added', $user->name, $user->get_key($alias));
}
